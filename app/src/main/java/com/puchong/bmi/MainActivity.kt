package com.puchong.bmi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.puchong.bmi.databinding.ActivityMainBinding
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.calculateButton.setOnClickListener{ calculateBMI() }
    }

    private fun calculateBMI() {
        val stringInWeight = binding.weightEditText.text.toString()
        val weight = stringInWeight.toDoubleOrNull()

        val stringInHeight = binding.heightEditText.text.toString()
        val height = stringInHeight.toDoubleOrNull()

        if(weight == null || weight == 0.0 || height == null || height == 0.0 ){
            binding.bodyResult.text = "Error"
            return
        }
        var bmi = weight/((height/100) * (height/100))
        val df = DecimalFormat("#.#")
        var formatBMI = df.format(bmi)
        binding.bmiResult.text = getString(R.string.bmi_result, formatBMI)

        diplsyBody(formatBMI)


    }

    private fun diplsyBody(formatBMI: String) {

        if (formatBMI.toDouble() < 18.5){
            binding.bodyResult.text = "UNDERWEIGHT"
        }else if(formatBMI.toDouble()  >= 18.5 || formatBMI.toDouble()  <= 24.9){
            binding.bodyResult.text = "NORMAL"
        }else if(formatBMI.toDouble()  >= 25 || formatBMI.toDouble()  <= 29.9){
            binding.bodyResult.text ="OVERWEIGHT"
        }else if(formatBMI.toDouble()  >30){
            binding.bodyResult.text = "OBESE"
        }
    }
}